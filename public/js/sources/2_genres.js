console.log('connected genres.js');

const data = {
  "genres": [
    {
      "id": 28,
      "name": "Action"
    },
    {
      "id": 12,
      "name": "Adventure"
    },
    {
      "id": 16,
      "name": "Animation"
    },
    {
      "id": 35,
      "name": "Comedy"
    },
    {
      "id": 80,
      "name": "Crime"
    },
    {
      "id": 99,
      "name": "Documentary"
    },
    {
      "id": 18,
      "name": "Drama"
    },
    {
      "id": 10751,
      "name": "Family"
    },
    {
      "id": 14,
      "name": "Fantasy"
    },
    {
      "id": 36,
      "name": "History"
    },
    {
      "id": 27,
      "name": "Horror"
    },
    {
      "id": 10402,
      "name": "Music"
    },
    {
      "id": 9648,
      "name": "Mystery"
    },
    {
      "id": 10749,
      "name": "Romance"
    },
    {
      "id": 878,
      "name": "Science Fiction"
    },
    {
      "id": 10770,
      "name": "TV Movie"
    },
    {
      "id": 53,
      "name": "Thriller"
    },
    {
      "id": 10752,
      "name": "War"
    },
    {
      "id": 37,
      "name": "Western"
    }
  ]
}

const genreCodes = [18, 10749, 14];

const getGenres = (genres) => {
  const list = data.genres;
  let genresarr = [];
  genres.map(genre => {
    list.map(item => {
      if(genre == item.id) {
        genresarr.push(item.name);
      } 
    });
  });
  return genresarr.join(', ');
};


const movieData = {
  "genres": [
    {
      "id": 28,
      "name": "Action"
    },
    {
      "id": 18,
      "name": "Drama"
    },
    {
      "id": 53,
      "name": "Thriller"
    }
  ],
}

const getMovieGenres = (genres) => {
  const list = genres;
  let genresarr = [];
    list.map(item => {
      genresarr.push(item.name);
    });
  return genresarr.join(', ');
};








// console.log(getGenres(genreCodes));
// let parsed = JSON.parse(data);